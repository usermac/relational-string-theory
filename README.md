May 23, 2018 Louisville, Kentucky, USA. — A new field naming convention that allowed me to create an online order system from scratch in 31 calendar days in late 2017. I call it Relational String Theory (RST). It removes all the old troubles I had experienced in developing such as the clients changing field names, typos, and more importantly—subjective field naming. It handles the linking of tables effortlessly. It is not for the casual drag and drop a spreadsheet onto FileMaker to get field names. It is for the bigger, more thoughtful projects. I've been using and improving it since 2007.

What's exciting is with FileMaker 17 you can use my system because of the new DefaltFields.xml. 

Two takeaways. One is table linking and the other is field names. Table linking is the easier of the two and the main reason I created the new system. Instead of guessing, I use the letter "t" to denote "table" and a number "1". So t1 represents table1. Name a table t1 in the define dialog. T1's key field is field t1. All the other tX's are foreign key id's. Table t2's key field name is t2 and so on. (The relationship 's "add a text note" narrative gives the table it's meaning.) Key fields are UUIDnumber but anything you want can be as always done. 

To link two tables. Table 1 (t1) id field is also named t1. It has a unique identifier. If you want to link to another table, simple put the value of table 1's t1 field into the corresponding field t1 of the other table. So if t1 is key field value is 1234 then put 1234 into table 6's t1 field. 

The next issues is end-user field names. I use "dX" for all fields. The "X" is the field number. Using the field's comment and Data Inspector's placeholder with the code "FieldComment (Get(FileName); GetFieldName(Self))." Credit HomeBase blog for this gem.

Oh, for the focus of the table I use field d21 and up. So it makes it really easy to know what any table is about just by looking at or above d21. 

Some observations about DefaultFields.xml: 

second tag must be <FMDefaultItems...

id="xx" is not required 

add comments using standard html comment tags

spaces don't matter

TagList equal to "1" will put the field on the layout. 

indexLanguage="English" can be changed to "Default" or "Unicode" or?

You can leave off whole tags such as "Validation" it will simply  take the defaults.

Oh, and my system? It is now trivial to find fields while developing. I admit it does fail for end-user UI. Of course they can't use it for sorts and the like so what you can do is simply add those fields with real names they can understand and then point back to the data in the dX field. Or you could design a UI that protects them from it. 

There is a lot I'm leaving off but I wanted to get this out. I've spent the past four days only getting my system into DefaultFields.xml. I hope to have time to properly document. You may use my new system at your own risk. 

The DefaultFields.xml can be found here: https://bitbucket.org/usermac/relational-string-theory/src/master/

Shields up!  - Brian Ginn

+_+_+_+_+_+_+_+_+_+_+_+_+_+
Introduction video is here: https://vimeo.com/271791959